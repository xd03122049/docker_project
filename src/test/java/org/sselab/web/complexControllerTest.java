package org.sselab.web;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;


import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class complexControllerTest {
    @Autowired
    TestRestTemplate restTemplate;
    @LocalServerPort
    int port;


    @Test
    public void receiveStringList() throws Exception {
        String[] strings = restTemplate.getForObject(UriComponentsBuilder.fromHttpUrl("http://localhost:"+port+"/strList")
                .queryParam("strList","hs")
                .queryParam("strList","ys")
                .queryParam("strList","ws")
                .queryParam("strList","zs").toUriString(), String[].class);
        assertThat(Arrays.asList(strings), is(Arrays.asList("hs", "ys", "ws", "zs")));
    }
}