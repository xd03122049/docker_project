package org.sselab;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;
import org.sselab.entity.Docker;

import javax.print.Doc;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DockerApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class DockerApplicationTest {
  @LocalServerPort
  private int port;

  public static final String PATH = "/docker";
  Logger logger = LoggerFactory.getLogger(DockerApplicationTest.class);
  @Autowired
  TestRestTemplate restTemplate;


  /**
   * 注意?value=value1里面的value1自带字符串,不需要加"",'否则也成了value1值的一部分!
   *
   * @throws Exception
   */
  @Test
  public void docker() throws Exception {
    Docker docker = restTemplate.getForObject(PATH + "?name=docker", Docker.class);
    logger.info("test get method\t" + docker.toString() + "\tis\tsuccessfully");
    assertThat(docker, is(new Docker("docker", "0")));
  }

  @Test
  public void setDocker() throws Exception {
    List<Docker> dockerList = Arrays.asList(new Docker("hs", "22"), new Docker("ys", "21"), new Docker("zs", "20"));
    Docker[] dockers = restTemplate.postForObject("/dockers/{name}/docker/{port}", dockerList, Docker[].class, "ys", 20);
    restTemplate.postForObject("/dockers/{name}/docker/{port}", dockerList, Docker[].class, "ys", 20);
    logger.info(Arrays.toString(dockers));
    assertThat(Arrays.asList(dockers), is(dockerList));
  }

  @Test
  public void updateDocker() throws Exception {
    restTemplate.put("/dockers/{name}/docker/{port}", new Docker("hs", "12"), "tian", port);
  }

  @Test
  public void deleteDocker() throws Exception {
    System.out.println(port);
    HttpHeaders headers = new HttpHeaders();
    headers.set("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);
    HttpEntity entity = new HttpEntity(headers);
    restTemplate.exchange(UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + PATH)
      .queryParam("name", "hs")
      .queryParam("port", 23).build().encode().toUri(), HttpMethod.DELETE, entity, Docker.class);
  }

  @Test
  public void test() {
    logger.info("=====================");
  }
}
