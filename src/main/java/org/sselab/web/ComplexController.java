package org.sselab.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ComplexController {
    Logger logger = LoggerFactory.getLogger(ComplexController.class);

    @GetMapping(value = "/strList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<String> receiveStringList(@RequestParam("strList") List<String> stringList) {
        logger.info(stringList.toString());
        return stringList;
    }
}
