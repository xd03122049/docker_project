package org.sselab.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Docker implements Serializable {
    private static final long serialVersionUID = -3247082489816859148L;
    private String name;
    private String port;
}
